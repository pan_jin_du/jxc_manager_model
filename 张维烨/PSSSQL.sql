/*==============================================================*/
/* Database name:  PSS                                          */
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2020/10/6 17:05:17                           */
/*==============================================================*/

IF (EXISTS(SELECT * FROM master.dbo.sysdatabases WHERE dbid=db_ID('PSS')))
DROP DATABASE PSS
go

/*==============================================================*/
/* Database: PSS                                                */
/*==============================================================*/
create database PSS
go

use PSS
go

/*==============================================================*/
/* Table: Commodity                                             */
/*==============================================================*/
create table Commodity (
   编号                   int                  identity,
   名称                   nvarchar(30)         not null,
   安全存量                 Int                  not null,
   当前数量                 int                  not null,
   建议采购价                Money                not null,
   建议销售价                Money                not null,
   最后一次采购日期             datetime             null,
   最后一次销售日期             datetime             null,
   constraint PK_COMMODITY primary key (编号)
)
go

/*==============================================================*/
/* Table: Contact                                               */
/*==============================================================*/
create table Contact (
   编号                   int                  identity,
   中文姓名                 nvarchar(5)          not null,
   英文姓名                 varchar(60)          null,
   联络人称谓                varchar(10)          null,
   电话                   varchar(20)          null,
   移动电话                 varchar(20)          null,
   供应商编号                int                  null,
   客户编号                 int                  null,
   constraint PK_CONTACT primary key (编号)
)
go

/*==============================================================*/
/* Table: Customer                                              */
/*==============================================================*/
create table Customer (
   编号                   int                  identity,
   公司简称                 nvarchar(10)         not null,
   公司全称                 varchar(60)          null,
   负责人姓名                varchar(30)          null,
   负责人称谓                varchar(30)          null,
   电话                   varchar(20)          null,
   传真                   varchar(20)          null,
   移动电话                 varchar(20)          null,
   公司地址                 varchar(100)         null,
   constraint PK_CUSTOMER primary key (编号)
)
go

/*==============================================================*/
/* Table: Employee                                              */
/*==============================================================*/
create table Employee (
   编号                   int                  not null,
   中文姓名                 nvarchar(5)          not null,
   英文姓名                 varchar(60)          null,
   电话                   varchar(20)          null,
   移动电话                 Varchar(20)          null,
   电子邮件                 varchar(50)          null,
   联络地址                 nvarchar(30)         null,
   constraint PK_EMPLOYEE primary key (编号)
)
go

/*==============================================================*/
/* Table: Purchase                                              */
/*==============================================================*/
create table Purchase (
   单号                   int                  identity,
   日期                   datetime             not null,
   客户编号                 int                  not null,
   商品编号                 int                  not null,
   单价                   money                not null,
   数量                   int                  not null,
   销售员编号                int                  null,
   constraint PK_PURCHASE primary key (单号)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('Purchase')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = '数量')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'Purchase', 'column', '数量'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '数量为正值表示采购；数量为负值表示采购退货',
   'user', @CurrentUser, 'table', 'Purchase', 'column', '数量'
go

/*==============================================================*/
/* Table: SaleInfo                                              */
/*==============================================================*/
create table SaleInfo (
   单号                   int                  identity,
   日期                   datetime             not null,
   供应商编号                int                  not null,
   商品编号                 int                  not null,
   单价                   money                not null,
   数量                   int                  not null,
   constraint PK_SALEINFO primary key (单号, 供应商编号, 商品编号)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('SaleInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = '数量')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'SaleInfo', 'column', '数量'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '数量为正值表示销售；数量为负值表示销售退货',
   'user', @CurrentUser, 'table', 'SaleInfo', 'column', '数量'
go

/*==============================================================*/
/* Table: ShipAddress                                           */
/*==============================================================*/
create table ShipAddress (
   编号                   int                  identity,
   所属编号                 int                  not null,
   送货地址                 varchar(100)         null,
   constraint PK_SHIPADDRESS primary key (编号)
)
go

/*==============================================================*/
/* Table: Supplier                                              */
/*==============================================================*/
create table Supplier (
   编号                   int                  identity,
   中文简称                 nvarchar(10)         not null,
   供应商全称                varchar(60)          null,
   负责人姓名                varchar(30)          null,
   负责人称谓                varchar(30)          null,
   电话                   varchar(20)          null,
   传真                   varchar(20)          null,
   移动电话                 varchar(20)          null,
   供应商地址                varchar(100)         null,
   工厂地址                 varchar(100)         null,
   最近一次进货日期             Datetime             null,
   constraint PK_SUPPLIER primary key (编号)
)
go

/*==============================================================*/
/* Table: UserInfo                                              */
/*==============================================================*/
create table UserInfo (
   编号                   int                  not null,
   登录名                  varchar(20)          not null,
   销售人员编码               int                  null,
   姓名                   nvarchar(5)          not null,
   密码                   varchar(30)          not null,
   标志                   bit                  not null,
   constraint PK_USERINFO primary key (登录名)
)
go

if exists(select 1 from sys.extended_properties p where
      p.major_id = object_id('UserInfo')
  and p.minor_id = (select c.column_id from sys.columns c where c.object_id = p.major_id and c.name = '标志')
)
begin
   declare @CurrentUser sysname
select @CurrentUser = user_name()
execute sp_dropextendedproperty 'MS_Description', 
   'user', @CurrentUser, 'table', 'UserInfo', 'column', '标志'

end


select @CurrentUser = user_name()
execute sp_addextendedproperty 'MS_Description', 
   '登录用户类型（0：管理员、1：销售员）',
   'user', @CurrentUser, 'table', 'UserInfo', 'column', '标志'
go

alter table Contact
   add constraint FK_CONTACT_REFERENCE_EMPLOYEE foreign key (供应商编号)
      references Employee (编号)
go

alter table Contact
   add constraint FK_CONTACT_REFERENCE_CUSTOMER foreign key (客户编号)
      references Customer (编号)
go

alter table Purchase
   add constraint FK_PURCHASE_REFERENCE_CUSTOMER foreign key (客户编号)
      references Customer (编号)
go

alter table Purchase
   add constraint FK_PURCHASE_REFERENCE_COMMODIT foreign key (商品编号)
      references Commodity (编号)
go

alter table Purchase
   add constraint FK_PURCHASE_REFERENCE_EMPLOYEE foreign key (销售员编号)
      references Employee (编号)
go

alter table SaleInfo
   add constraint FK_SALEINFO_REFERENCE_SUPPLIER foreign key (供应商编号)
      references Supplier (编号)
go

alter table SaleInfo
   add constraint FK_SALEINFO_REFERENCE_COMMODIT foreign key (商品编号)
      references Commodity (编号)
go

alter table ShipAddress
   add constraint FK_SHIPADDR_REFERENCE_CUSTOMER foreign key (所属编号)
      references Customer (编号)
go

alter table UserInfo
   add constraint FK_USERINFO_REFERENCE_EMPLOYEE foreign key (销售人员编码)
      references Employee (编号)
go

